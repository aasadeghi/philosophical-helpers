#!/bin/bash

used_disk_percentage=$(echo $(df --output=pcent /dev/mapper/ubuntu--vg-ubuntu--lv) | sed -e "s/[^0-9]*//g");

if ((used_disk_percentage > 85))
then
    docker image prune -f --filter="until=24h"
    docker builder prune -f --filter="until=24h"
    docker container prune -f --filter="until=24h"
else
    date=$(date)
    echo "$date: $used_disk_percentage% of disk is used. No need to clean up!"
fi
